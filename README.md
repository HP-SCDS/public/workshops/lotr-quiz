# Go Guide

## Project layout

This repository itself should be used as a guide for project layout.
Each folder in this repo should have its own README.md explaining what it
is about and how it can be used.
As general rule follow what's documented in [golang-standards/project-layout](https://github.com/golang-standards/project-layout)

## Dev env setup

The intent here is to unify versions of go and other tools and also to provide
information on how to install and use them on development environment.

### Go version

Latest stable version is 1.13.4. We need newer go version to be able to use
go modules to manage dependencies. NOTE: since go adopted modules officially
we will not be using "dep" anymore.

```
  # go version
  go version go1.13.4 linux/amd64
```

### How to install go

It might be different on different OS.

#### Linux

There are (at least) 2 options to install go at needed version on latest Ubuntu (19.04).

- Via Snap

```
# snap install go
```

- Manually

Following the steps described on the link bellow

- https://golang.org/doc/install?download=go1.13.4.linux-amd64.tar.gz

NOTE: it will automatically download go v1.13.4.

#### Windows

https://dl.google.com/go/go1.13.4.windows-amd64.msi

### IDE

[VSCode](https://code.visualstudio.com/docs/languages/go)

[Goland](https://www.jetbrains.com/go/)

#### Configure GoLand with golint

Steps

- Install golint `go get go get -u golang.org/x/lint/golint`
- Configure Goland:
  - Go to Settings > Tools > File Watchers
  - Add (+) new custom watcher
  - Give it a name
  - Select Go file type
  - Scope Project Files
  - Program is golint (assuming you installed it and $GOPATH/bin is in your $PATH, browse it otherwise)
  - Arguments: `--set_exit_status $FilePath$`
  - Output paths to refresh: `$FilePath$`
  - Save

Now every time you change a file, golint will run and start complaining!

## Dependency management

Go Modules

- https://github.com/golang/go/wiki/Modules
- https://blog.golang.org/using-go-modules

NOTE: For Rubyists, go mod is somehow equivalent to bundler. It provides a powerful tool to manage packages and their
versions as proposed here https://go.googlesource.com/proposal/+/master/design/24301-versioned-go.md

### Problems fetching GHE dependencies when running 'go build'

Try (if you're ok having git fetching repos over ssh instead of https):

```
git config --global --add url."git@github.azc.ext.hp.com:".insteadOf "https://github.azc.ext.hp.com/"
```

Or, if you prefer over HTTPS with auth token instead:

```
git config --global url."https://${GITHUB_TOKEN}:x-oauth-basic@github.azc.ext.hp.com/".insteadOf "https://github.azc.ext.hp.com/"
```

## Training

### Starter

- https://tour.golang.org/welcome
- https://medium.com/devnetwork/golang-for-ruby-developers-c0a0ce19e367

## Code style

Must read docs after basic Go knowledge is achieved:

- https://golang.org/doc/effective_go.html
- https://github.com/golang/go/wiki/CodeReviewComments
