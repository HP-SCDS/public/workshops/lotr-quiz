package bootstrap

import (
	"lotrquiz/api"
	"lotrquiz/quiz"
)

// Bootstrap exports the necessary objects to run the server.
type Bootstrap struct {
	Server *api.Server
}

// New initialize and injects all the dependencies.
func New() *Bootstrap {

	quiz := quiz.New()

	server := api.NewServer(api.Config{
		Address: ":8080",
		Quiz:    quiz})

	return &Bootstrap{
		Server: server,
	}
}
