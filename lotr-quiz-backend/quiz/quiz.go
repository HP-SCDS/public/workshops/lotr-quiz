package quiz

import (
	"math/rand"
	"time"
)

// Quiz handle quiz logic
type Quiz struct {
	questions []*Question
}

// New creates a quiz instance
func New() *Quiz {
	rand.Seed(time.Now().UnixNano())
	return &Quiz{
		questions: questions,
	}
}

// GetQuestions returns all quiz questions
func (q *Quiz) GetQuestions() []*Question {
	return q.questions
}

// GetQuestion returns a quiz question
func (q *Quiz) GetQuestion() *Question {
	return q.questions[rand.Intn(len(q.questions))]
}

// IsValidAnswer verify that a question is correct
func (q *Quiz) IsValidAnswer(questionID string, answerID string) bool {
	for _, question := range q.questions {
		if question.ID == questionID {
			for _, answer := range question.Answers {
				if answer.ID == answerID {
					return answer.IsCorrect
				}
			}
		}
	}
	return false
}
