package main

import "lotrquiz/api/bootstrap"

func main() {
	b := bootstrap.New()
	b.Server.Listen()
}
